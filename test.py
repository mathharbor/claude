import hashlib
m = u'shilpi'
print "The plaintext message is: %s" %m
print "================================"
h1 = hashlib.sha1(m).hexdigest()
print "The SHA1 hash is: %s" %h1
h224 = hashlib.sha224(m).hexdigest()
print "The SHA224 hash is: %s" %h224
h256 = hashlib.sha256(m).hexdigest()
print "The SHA256 hash is: %s" %h256
h384 = hashlib.sha384(m).hexdigest()
print "The SHA384 hash is: %s" %h384
h512 = hashlib.sha512(m).hexdigest()
print "The SHA512 hash is: %s" %h512